from tensorflow.keras.utils import to_categorical
import pandas as pd
from os import listdir, path, makedirs
import numpy as np


def load_file(filepath):
    return pd.read_csv(filepath,delim_whitespace=True).values

def load_directory(path):
    filenames = listdir(path)
    filenames.sort()
    files = []
    for filename in filenames:
        files.append(load_file(path+filename))
    return np.dstack(files)

def load_dataset_part(part):
    prefix = "./dataset/"
    X = load_directory(prefix + part+'/Inertial Signals/')
    Y = load_file(prefix + part + '/y_' + part + '.txt')
    return X, Y

def load_dataset():
    X_train, Y_train = load_dataset_part('train')
    X_test, Y_test = load_dataset_part('test')
    Y_train, Y_test = Y_train - 1, Y_test - 1
    Y_train, Y_test = to_categorical(Y_train), to_categorical(Y_test)
    print(X_train.shape, Y_train.shape, X_test.shape, Y_test.shape)
    return X_train, Y_train, X_test, Y_test

def safe_make_dir(dir):
    if not path.exists(dir):
        makedirs(dir)