##### Human activity recognition using smartphones data

Project aim is to test performance of different models in recognising human activity based on Samsung smartphones accelerometer sensor data.

More information about the dataset at: https://archive.ics.uci.edu/ml/datasets/human+activity+recognition+using+smartphones

##### Requirements

Python environment requirements are listed in `environment_droplet.yml`


##### Usage

To run Multi Layer Perceptron with default arguments simply run the following command:

`python recognition.py` 

Currently available models include Multi Layer Perceptron (`MLP`), Multi Head Convolutional Neural Network (`MultiHeadCNN1D`) and Deep LSTM (`DeepLSTM`)  

To see a list of available model hyperparameters run:

`python recognition.py -h` 

