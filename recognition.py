import argparse
import sys
from utils import load_dataset, safe_make_dir
from models.BaseModel import BaseModel
from models.MLP import MLP
from models.MultiHeadCNN1D import MultiHeadCNN1D
from models.DeepLSTM import DeepLSTM
from numpy import mean, std
from tensorflow.keras.models import load_model


# fit and evaluate a model
def fit_evaluate_model(X_train, Y_train, X_test, Y_test, ModelClass, output_dir, lr=0.001, heads=1, batch_size=32,
                       epochs=10, verbose=0, restore=False, **model_params):
    model_chkp = output_dir + ModelClass.__name__ + '_model.hdf5'
    n_timesteps, n_features, n_outputs = X_train.shape[1], X_train.shape[2], Y_train.shape[1]
    if restore:
        try:
            model = load_model(model_chkp)
        except (FileNotFoundError, OSError):
            print("Checkpoint not found. Starting training from scratch...")
            model = ModelClass(n_timesteps, n_features, n_outputs, lr, heads, **model_params)
    else:
        model = ModelClass(n_timesteps, n_features, n_outputs, lr, heads, **model_params)
    model.fit(X_train, Y_train, model_chkp, output_dir, batch_size, epochs, verbose, validation_data=[X_test, Y_test])
    model = load_model(model_chkp)
    _, accuracy = model.evaluate(X_test, Y_test, batch_size, verbose)
    return accuracy


def experiment_stats(n_times, mean, std):
    print(f"Mean(std) accuracy from {n_times} runs: {mean:.3f}({std:.3f})%")


# run the experiments
def run_experiment(n_times=1, ModelClass=BaseModel, lr=0.001, heads=1, verbose=0, epochs=10, batch_size=32,
                   output_dir='./output/', restore=False, **model_params):
    safe_make_dir(output_dir)
    X_train, Y_train, X_test, Y_test = load_dataset()
    results = []
    for n in range(n_times):
        result = fit_evaluate_model(X_train, Y_train, X_test, Y_test, ModelClass, output_dir, lr, heads,
                                    batch_size, epochs, verbose, restore, **model_params)
        results.append(result * 100.0)
        print(f"Model: {ModelClass.__name__} | Run: {n + 1} | Accuracy: {result:.3f}")
    m, n = mean(results), std(results)
    experiment_stats(n_times, m, n)


# parse commandline arguments
def parse_args(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', default='./output/', type=str, help='Folder for outputing data.')
    parser.add_argument('-r', '--restore', default=False, type=bool, help='Whether to restore training from checkpoint.')
    parser.add_argument('-t', '--times', type=int, default=1,
                        help='How many times to run experiment to average results.')
    parser.add_argument('-v', '--verbose', type=int, default=1, help='Level of verbosity in model training/validation.')
    parser.add_argument('-m', '--model', default='MLP', help='Model architecture.')
    parser.add_argument('-b', '--batch_size', type=int, default=64, help='Batch size.')
    parser.add_argument('-e', '--epochs', type=int, default=10, help='Number of epochs for training.')
    parser.add_argument('-lr', '--learning_rate', type=float, default=0.001, help='Learning rate for training.')
    parser.add_argument('-he', '--heads', default=1, help='Number of heads in the MultiHeadCNN1D model.')
    parser.add_argument('-du', '--dense_units', type=int, default=300, help='Number of dense units in the MLP model.')
    parser.add_argument('-dr', '--dropout', type=float, default=0.5, help='Dropout rate for non MLP models.')
    parser.add_argument('-f', '--filters', type=int, default=64, help='Number of filters in the MultiHeadCNN1D model.')
    parser.add_argument('-ks', '--kernel_size', type=int, default=3, help='Size of kernel in the MultiHeadCNN1D model.')
    parser.add_argument('-lu', '--lstm_units', type=int, default=64,
                        help='Number of LSTM units in each layer of DeepLSTM model.')
    argspace = parser.parse_args(argv)
    return argspace


if __name__ == '__main__':
    args = parse_args()
    if args.model == 'MLP':
        run_experiment(args.times, ModelClass=MLP, lr=args.learning_rate, heads=args.heads, verbose=args.verbose,
                       epochs=args.epochs, batch_size=args.batch_size, output_dir=args.output,restore=args.restore,
                       dense_units=args.dense_units)
    elif args.model == 'MultiHeadCNN1D':
        run_experiment(args.times, ModelClass=MultiHeadCNN1D, lr=args.learning_rate, heads=args.heads,
                       verbose=args.verbose, epochs=args.epochs, batch_size=args.batch_size, output_dir=args.output,
                       restore=args.restore, filters=args.filters, kernel_size=args.kernel_size, dropout=args.dropout)
    elif args.model == 'DeepLSTM':
        run_experiment(args.times, ModelClass=DeepLSTM, lr=args.learning_rate, heads=args.heads, verbose=args.verbose,
                       epochs=args.epochs, batch_size=args.batch_size, output_dir=args.output, restore=args.restore,
                       lstm_units=args.lstm_units,dropout=args.dropout)
