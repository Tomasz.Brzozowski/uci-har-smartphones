from models.BaseModel import BaseModel
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Dense, LSTM


class DeepLSTM(BaseModel):
    def build_model(self):
        inputs = Input(shape=(self.n_timesteps, self.n_features))
        lstm = LSTM(self.model_params['lstm_units'],dropout=self.model_params['dropout'],return_sequences=True)(inputs)
        lstm = LSTM(self.model_params['lstm_units'],dropout=self.model_params['dropout'])(lstm)
        outputs = Dense(100, activation='relu')(lstm)
        outputs = Dense(self.n_outputs, activation='softmax')(outputs)
        model = Model(inputs=inputs, outputs=outputs)
        return model