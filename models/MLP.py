from models.BaseModel import BaseModel
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Dropout, Flatten, Dense


class MLP(BaseModel):
    def build_model(self):
        inputs = Input(shape=(self.n_timesteps, self.n_features))
        input_flattened = Flatten()(inputs)
        dropout = Dropout(0.1)(input_flattened)
        dense = Dense(self.model_params['dense_units'], activation='relu')(dropout)
        dropout  = Dropout(0.2)(dense)
        dense = Dense(self.model_params['dense_units'], activation='relu')(dropout)
        dropout  = Dropout(0.2)(dense)
        dense = Dense(self.model_params['dense_units'], activation='relu')(dropout)
        dropout = Dropout(0.3)(dense)
        outputs = Dense(self.n_outputs, activation='softmax')(dropout)
        model = Model(inputs=inputs, outputs=outputs)
        return model