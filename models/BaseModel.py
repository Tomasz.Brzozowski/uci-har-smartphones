from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint,TensorBoard


class BaseModel():
    def __init__(self, n_timesteps, n_features, n_outputs, lr=0.001, heads=1, **model_params):
        super().__init__()
        self.adam = Adam(lr)
        self.heads = heads
        self.n_timesteps = n_timesteps
        self.n_features = n_features
        self.n_outputs = n_outputs
        self.model_params = model_params
        self.init_model()

    def build_model(self):
        raise NotImplementedError

    def compile_model(self):
        self.model.compile(optimizer=self.adam, loss='categorical_crossentropy',
                           metrics=['accuracy'])

    def init_model(self):
        self.model = self.build_model()
        self.compile_model()

    def fit(self, x_train, y_train, chkp_filepath, output_dir, batch_size=32, epochs=10, verbose=1, validation_data=None):
        model_chkp = ModelCheckpoint(filepath=chkp_filepath, verbose=1, save_best_only=True, monitor='val_accuracy')
        tensorboard_chkp = TensorBoard(log_dir=output_dir)
        self.model.fit(x_train, y_train, batch_size, epochs, verbose, callbacks = [model_chkp, tensorboard_chkp],
                       validation_data=validation_data)

    def evaluate(self, x_test, y_test, batch_size=32, verbose=1):
        self.model.evaluate(x_test, y_test, batch_size=batch_size, verbose=verbose)
