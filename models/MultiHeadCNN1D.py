from models.BaseModel import BaseModel
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Conv1D, Dropout, MaxPooling1D, Flatten, Dense, concatenate


class MultiHeadCNN1D(BaseModel):
    def build_model(self):
        inputs = []
        heads = []
        for _ in range(self.heads):
            input = Input(shape=(self.n_timesteps, self.n_features))
            conv = Conv1D(filters=self.model_params['filters'], kernel_size=self.model_params['kernel_size'],
                          activation='relu')(input)
            dropout = Dropout(self.model_params['dropout'])(conv)
            pool = MaxPooling1D(pool_size=2)(dropout)
            flat = Flatten()(pool)
            inputs.append(input)
            heads.append(flat)
        if self.heads > 1:
            outputs = concatenate(heads)
        else:
            outputs = heads[0]
        outputs = Dense(100, activation='relu')(outputs)
        outputs = Dense(self.n_outputs, activation='softmax')(outputs)
        model = Model(inputs=inputs, outputs=outputs)
        return model